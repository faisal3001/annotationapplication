import { Component, ViewChild, ElementRef, Input, AfterViewInit } from '@angular/core';

import { Observable } from 'rxjs';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/takeUntil';
import 'rxjs/add/operator/pairwise';
import 'rxjs/Rx';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit {
  title = 'app';
  imageWidth: number = 400;
  imageheight: number = 400;

  context: CanvasRenderingContext2D;
  imageFile: any;
  @ViewChild("canvas") mycanvas;
  @ViewChild("copyCanvas") copyCanvas;
  preview(e: any): void {
    let canvas = this.mycanvas.nativeElement;
    let context = canvas.getContext('2d');
    context.clearRect(0, 0, this.width, this.height);

    let render = new FileReader();
    render.onload = function (event: any) {
      let img = new Image();
      img.onload = function () {
        canvas.width = img.width;
        canvas.height = img.height;
        context.drawImage(img, 0, 0);
      }
      img.src = event.target.result;
    }
    render.readAsDataURL(e.target.files[0]);
    //this.imageFile = render.readAsDataURL(e.target.files[0]);
    setTimeout(() => {
      this.saveRestoreImage();
      this.width = canvas.width;
      this.height = canvas.height;
      console.log("Height and Width: ", this.width, this.height);
    }, 100);
  }

  heightAndWidth() {

  }

  @ViewChild('canvas') public canvas: ElementRef;

  public width = 400;
  public height = 400;

  private cx: CanvasRenderingContext2D;
  public ngAfterViewInit() {
    const canvasEl: HTMLCanvasElement = this.canvas.nativeElement;
    this.cx = canvasEl.getContext('2d');

    canvasEl.width = this.width;
    canvasEl.height = this.height;

    this.cx.lineWidth = 3;
    this.cx.lineCap = 'round';
    this.cx.strokeStyle = this.colorOfLine.nativeElement.value;
    this.captureEvents(canvasEl);
    this.saveRestoreImage();
  }

  private captureEvents(canvasEl: HTMLCanvasElement) {
    let drawing = false;
    Observable.fromEvent(canvasEl, 'mouseup').subscribe(res => {
      if (drawing) {
        drawing = false;
        this.saveRestoreImage();
        //console.log('save image');
      }
    })
    Observable.fromEvent(canvasEl, 'mousedown').switchMap(() => {
      return Observable.fromEvent(canvasEl, 'mousemove')
        .takeUntil(Observable.fromEvent(canvasEl, 'mouseup'))
        .takeUntil(Observable.fromEvent(canvasEl, 'mouseleave'))
        .pairwise();
    }).subscribe((res: [MouseEvent, MouseEvent]) => {
      drawing = true;
      const rect = canvasEl.getBoundingClientRect();
      const prevPos = {
        x: res[0].clientX - rect.left,
        y: res[0].clientY - rect.top
      };

      const currentPos = {
        x: res[1].clientX - rect.left,
        y: res[1].clientY - rect.top
      };

      this.drawOnCanvas(prevPos, currentPos);
      //console.log('Down & Move',res);
    });
  }

  private drawOnCanvas(prevPos: { x: number, y: number }, currentPos: { x: number, y: number }) {

    if (!this.cx) { return; }

    this.cx.beginPath();

    if (prevPos) {
      this.cx.moveTo(prevPos.x, prevPos.y); // from
      this.cx.lineTo(currentPos.x, currentPos.y);
      this.cx.stroke();
    }
  }
  tempArray = [];
  restorePoints = [];
  redoArray = [];
  saveRestoreImage() {
    let canvasSrc = this.canvas.nativeElement.toDataURL("image/png")
    //let imageSrc = canvasSrc;
    this.restorePoints.push(canvasSrc);
    console.log("image saved : ", canvasSrc);
  }
  saveCanvasAs(canvas, fileName) {
    let
      canvasDataUrl = canvas.nativeElement.toDataURL("image/png")
        .replace(/^data:image\/[^;]*/, 'data:application/octet-stream'),
      link = document.createElement('a'); // create an anchor tag

    // set parameters for downloading
    link.setAttribute('href', canvasDataUrl);
    link.setAttribute('target', '_blank');
    //link.setAttribute('type', 'png');
    link.setAttribute('download', fileName);

    // compat mode for dispatching click on your anchor
    if (document.createEvent) {
      let evtObj = document.createEvent('MouseEvents');
      evtObj.initEvent('click', true, true);
      link.dispatchEvent(evtObj);
    } else if (link.click) {
      link.click();
    }
  }
  downloadImage() {
    this.saveCanvasAs(this.canvas, 'export.png');
  }

  @ViewChild('selectedColor') colorOfLine;
  selectedColorName(e: any) {
    console.log("Color Picker", this.colorOfLine.nativeElement.value);
    this.cx.strokeStyle = this.colorOfLine.nativeElement.value;
  }
  @ViewChild('lineWidth') lineWidth;
  changeLineWidth() {
    console.log("line WIdth: ", this.lineWidth);
    this.cx.lineWidth = this.lineWidth.nativeElement.value;

  }

  canvasUndo() {
    if (this.restorePoints.length > 0) {
      console.log("Undo Image Array Length : ", this.restorePoints.length);
      let tempImage = this.restorePoints.pop();
      this.redoArray.push(tempImage);
      var image = new Image();
      console.log("undo image : ", this.restorePoints[this.restorePoints.length - 1]);
      let e = this.restorePoints[this.restorePoints.length - 1];
      image.src = e;
      this.cx.clearRect(0, 0, this.width, this.height);
      this.cx.drawImage(image, 0, 0, this.width, this.height);
    }
    else {
      console.log("else Condition undo");
    }
  }
  canvasRedo() {
    if (this.redoArray.length > 0) {
      console.log("Redo Image Array Length : ", this.redoArray.length);
      let tempImage = this.redoArray.pop();
      this.restorePoints.push(tempImage);
      console.log("redo image : ", this.restorePoints[this.restorePoints.length - 1]);
      var image = new Image();
      let e = this.restorePoints[this.restorePoints.length - 1];
      image.src = e;
      this.cx.clearRect(0, 0, this.width, this.height);
      this.cx.drawImage(image, 0, 0, this.width, this.height);
    }
    else {
      console.log("else condition redo");
    }
  }
  clearCanvas() {
    this.cx.clearRect(0, 0, this.width, this.height);
  }

}